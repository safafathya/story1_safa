from .views import story
from django.urls import path

urlpatterns = [
	path('', story, name='story'),
]